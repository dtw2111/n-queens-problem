def find_positions(n):
    board = []
    # build n x n empty board
    for i in range(n):
        row = [0] * n
        board.append(row)
    
    place_queen_in_row(board, 0)
    print(board)
    return [row.index(1) for row in board]

def queen_is_safe(board, row, col):
    # check columns and diagonals going row by row
    for row_idx in range(len(board)):
        if row_idx == row:
            continue # skip row where we just placed a queen
        if board[row_idx][col] == 1: # make sure column is clear
            return False
        # check diagonal positions
        if col - abs(row - row_idx) >= 0:
            if board[row_idx][col - abs(row - row_idx)] == 1:
                return False # check left diagonal in row if it exists
        if col + abs(row - row_idx) < len(board):
            if board[row_idx][col + abs(row - row_idx)] == 1:
                return False # check right diagonal in row if it exists

    return True # if queen is not in path of any unsafe squares

def place_queen_in_row(board, row):
    if row >= len(board):
        return True # we're done when we've passed the last row
    
    for col in range(len(board)):
        if queen_is_safe(board, row, col):
            board[row][col] = 1 # place new queen
            if place_queen_in_row(board, row + 1):
                return True
        board[row][col] = 0

    return False